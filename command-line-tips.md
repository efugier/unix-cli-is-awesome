## Getting help
* `man cat`
* `help cd`
* `jq --help`, `jq -h`


## Juggle with commands

### Editing
* `<C-/>` undo
* `<C-a>` go to the beginning of the line
* `<C-e>` go to the end of the line
* `<C-k>` delete from cursor to the end of the command line
* `<C-u>` delete from cursor to the beginning of the command line
* `<C-r>` triggers history search
* `<C-w>` delete from cursor to beginning of the word
* `<C-y>` paste word or text that was cut using one of the deletion shortcuts (such as the one above) after the cursor
* `<C-x-x>` move between start of command line and current cursor position (and back again)
* `<Alt>b` move backward one word (or go to start of word the cursor is currently on)
* `<Alt>f` move forward one word (or go to end of word the cursor is currently on)
* `<Alt>d` delete to end of word starting at cursor (whole word if cursor is at the beginning of word)

### Recall
* `<Esc>.` or `<Alt>.` adds the last command's last word
* `<C-r>` search the history backwards
* `<C-g>` escape from history searching mode
* `<C-p>` previous command in history (i.e. walk back through the command history)
* `<C-n>` next command in history (i.e. walk forward through the command history)

### Control
* `<C-l>` clear the screen
* `<C-s>` stops the output to the screen (for long running verbose command)
* `<C-q>` allow output to the screen (if previously stopped using command above)
* `<C-c>` terminate the command
* `<C-z>` suspend/stop the command use `bg` to run it in background, `jobs` to list background jobs and `fg` to bring them back
* `&` after a command to run it in background
* `nohup` before a command to detach it's life cyle from the current terminal 
* `&>/dev/null` redirects the output of the process to `/dev/null`, use `&>/dev/null/ &` after a command to ignore its output and run it in background
* `nohup command &>/dev/null &` hence creates a totally independent process, `command </dev/null &>/dev/null &` achieves the same goal by instantly sending EOF to the program (using this you won't see the `Done` after the backgrounded process ends)

### Bang !
* `!!` run last command
* `!blah` run the most recent command that starts with ‘blah’ (e.g. !ls)
* `!blah:p` print out the command that !blah would run (also adds it as the latest command in the command history)
* `!$` the last word of the previous command (same as Alt + .)
* `!$:p` print out the word that !$ would substitute
* `!*` the previous command except for the first word (e.g. `find a b` gives `a b`)
* `!*:p` print out what !* would substitute


### Black magic
* `^foo^bar` same as `!!:s/foo/bar/`, replace `foo` with `bar` in the last command and run it
* Go up in history and hit `<C-o>`, this will execute the picked command prepare the following one in the prompt


## Work with files

* `> my-file` overwrites the file's content
* `>> my-file` appends to the file's existing content
* `< my-file` outputs the file's content, same as `cat my-file |` but without starting an additional process
* `mkdir my-new-directory` make a new directory

All the following commands support `-v` for verbose and `-i` for interactive

* `mv my-file new-file` moves a file
* `cp my-file new-file` copies a file
* `rm my-file`, `rm -r my-directory` remove a file or directory


## Combining tools

Pipes !
* `seq 100 | grep 3 | wc -l` counts in how many numbers "3" appears between 1 and 100

## Making tools

### Binary executable
Make them in a compiled language such as Rust, go, C...

### Interpreted scripts
Use the first line of the file to describe how to run it:

```python
#!/usr/bin/env python3

def factorial(n):
    res = 1
    for i in range(2, n + 1):
        res *= i

    return res

if __name__ == "__main__":
    import sys
    n = int(sys.argv[1])
    print(factorial(n))
```

```bash
$ ./factorial.py 5
120
```

### Shell functions

```bash
$ factorial() { echo 1; seq $1 | paste -s -d\* | bc; }
$ fac 5
120
```
Can be put in `.bashrc` to be reusable.


### Aliases

```bash
alias la='ls -1a --group-directories-first'
```
Can be put in `.bashrc` or `zshrc` to be reusable.


## Sources
* [Unix / Linux Cheat Sheet](http://cheatsheetworld.com/programming/unix-linux-cheat-sheet/)
* [Bash shortcuts for maximum productivity](https://www.skorks.com/2009/09/bash-shortcuts-for-maximum-productivity/)
* [Doing Data Science at the Command Line](https://www.datascienceatthecommandline.com/)
* [Running Bash Commands in the Background the Right Way](https://www.maketecheasier.com/run-bash-commands-background-linux/)

# Useful bits
* Make an offline mirror of a site `wget -mkEpnp http://example.org`, short version of `--mirror --convert-links --adjust-extension --page-requisites --no-parent`
* Repeat a command for every line of a file: `while read in; do echo "$in"; done < file.txt`
* make a quick backup: `tar -zcC /my/path my-folder > ~/somewhere/my-dir-backupbackup-$(date +%s).tar.gz`
